import axios from 'axios';

const URL_SERV = 'https://rickandmortyapi.com/api';

export const getCharacters = async (prevState, page = 1) => {
    try {
        const response = await axios.get(`${URL_SERV}/character/?page=${page}`)
        return {
            result: prevState.result ? [...prevState.result, ...response.data.results] : response.data.results,
            page: page,
            // end: response.data.results.length === 0 ? true : false
        }
    } catch (e) {
        throw e
    }
}

export const getCharacter = async (id = 1) => {
    try {
        const loading = false
        const response = await axios.get(`${URL_SERV}/character/${id}`);
        return {
            characterId: response.data.id,
            data: response.data,
            loading: !response.data ? true : false
        };
    } catch (e) {
        throw e
    }
}

export const addNewsletter = async (data) => {
    try {
        const findUser = await axios.get(`${URL_SERV}/newsletter?email=${data.email}`);

        if (!Array.isArray(findUser.data) || !findUser.data.length) {
            // add user
            const response = await axios({
                method: 'POST',
                url: `${URL_SERV}/newsletter`,
                data: {
                    email: data.email
                }
            });

            return {
                newsletter: 'added',
                email: response.data
            }
        } else {
            // already on the db
            return {
                newsletter: 'failed'
            }
        }
    } catch(error){
        throw error;
    }
}


import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'


const Header = () => {
    return(
        <>
            <Navbar className="yellotail">
                <LinkContainer to="/">
                    <Navbar.Brand>//TODO something interesting</Navbar.Brand>
                </LinkContainer>
            </Navbar>
            <Nav>
                <Nav.Item>
                    <LinkContainer to="/characters">
                        <Nav.Link>Characters</Nav.Link>
                    </LinkContainer>
                </Nav.Item>
                <Nav.Item>
                    <LinkContainer to="/forms">
                        <Nav.Link>Forms</Nav.Link>
                    </LinkContainer>
                </Nav.Item>
            </Nav>
        </>
    )
}

export default Header;
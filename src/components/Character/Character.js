import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {clearPostById, getCharacter} from '../../store/actions';
import {showToast} from '../utils/tools';

import Moment from 'react-moment';
import {LinearProgress} from "@material-ui/core";

const Character = (props) => {
    const character = useSelector(state => state.character.data.data);
    const [loading, setLoading] = useState(false)
    const dispatch = useDispatch();

    useEffect(() => {
        if (props.match.params.id) {
            dispatch(getCharacter(props.match.params.id))
        } else {
            props.history.push('/');
        }
    }, [dispatch, props.match.params.id])

    useEffect(() => {
        if (!character && character === undefined) {
            showToast('ERROR', 'The page you request in not available');
            props.history.push('/');
        }
    }, [character, props.history])


    useEffect(() => {
        return () => {
            dispatch(clearPostById())
        }
    }, [dispatch])

    return (
        <>
            {character ?
                <>
                    <div>
                        <h1>{character.name}</h1>
                        <img src={character.image} alt={`${character.name} photo`}/>
                    </div>
                    <div className="author">
                        On <Moment format="DD MM YYYY">
                        {character.created}
                    </Moment>
                        <p> Status: <span>{character.status} </span></p>

                    </div>
                    <div className="mt-3 content">
                        <div dangerouslySetInnerHTML={
                            {__html: character.species}
                        }>

                        </div>
                    </div>
                </>

                :
                <LinearProgress/>}
        </>
    )
}


export default Character;
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getCharacters} from '../../store/actions';
import {
    withStyles,
    MuiThemeProvider,
    createMuiTheme
} from "@material-ui/core/styles";
import {Spinner, Button} from 'react-bootstrap';
import Masonry from 'react-masonry-css'
import Moment from "react-moment";
import {LinkContainer} from 'react-router-bootstrap';
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Divider from "@material-ui/core/Divider";
import {Card} from "@material-ui/core";


const Characters = () => {
    const faces = [
        "http://i.pravatar.cc/300?img=1",
        "http://i.pravatar.cc/300?img=2",
        "http://i.pravatar.cc/300?img=3",
        "http://i.pravatar.cc/300?img=4"
    ];
    const muiBaseTheme = createMuiTheme();

    const theme = {
        overrides: {
            MuiCard: {
                root: {
                    "&.MuiEngagementCard--01": {
                        transition: "0.3s",
                        maxWidth: 300,
                        margin: "auto",
                        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
                        "&:hover": {
                            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
                        },
                        "& .MuiCardMedia-root": {
                            paddingTop: "56.25%"
                        },
                        "& .MuiCardContent-root": {
                            textAlign: "left",
                            padding: 15
                        },
                        "& .MuiDivider-root": {
                            margin: `10px 0`
                        },
                        "& .MuiTypography--heading": {
                            fontWeight: "bold"
                        },
                        "& .MuiTypography--subheading": {
                            lineHeight: 1.8
                        },
                        "& .MuiAvatar-root": {
                            display: "inline-block",
                            border: "2px solid white",
                            "&:not(:first-of-type)": {
                                marginLeft: -20
                            }
                        }
                    }
                }
            }
        }
    };
    const characters = useSelector(state => state.character);
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        dispatch(getCharacters({}, 1))
    }, [dispatch])

    const loadMorePost = () => {
        setLoading(true);
        const page = characters.page + 1;
        dispatch(getCharacters(characters, page)).then(() => {
            setLoading(false);
        })
    }
    return (
        <>        <MuiThemeProvider theme={createMuiTheme(theme)}>
            <Masonry
                breakpointCols={{default: 4, 800: 3, 400: 1}}
                className="my-masonry-grid"
                columnClassName="my-masonry-grid_column"
            >
                {characters.result ?
                    characters.result.map((item) => {
                        return (
                            <div key={item.id}>
                                <Card className={"MuiEngagementCard--01"}>
                                    <CardMedia
                                        className={"MuiCardMedia-root"}
                                        image={item.image}
                                    />
                                    <div className="author">
                                        <span>{item.id} -</span>
                                        <Moment format="DD MMMM">
                                            {item.created}
                                        </Moment>
                                    </div>
                                    <CardContent className={"MuiCardContent-root"}>
                                        <Typography
                                            className={"MuiTypography--heading"}
                                            variant={"h6"}
                                            gutterBottom
                                        >{item.name}
                                        </Typography>
                                        <Typography
                                            className={"MuiTypography--subheading"}
                                            variant={"caption"}
                                        >
                                            <h6>
                                                Status: Alive
                                            </h6>
                                        </Typography>
                                        <Typography
                                            className={"MuiTypography--subheading"}
                                            variant={"caption"}
                                        >
                                            <h6>Species: Human</h6>
                                        </Typography>
                                        <Typography
                                            className={"MuiTypography--subheading"}
                                            variant={"caption"}
                                        >
                                            <h6> Location:Earth (Replacement Dimension)</h6>
                                        </Typography>
                                        <Typography
                                            className={"MuiTypography--subheading"}
                                            variant={"caption"}
                                        >
                                            <h6>Персонажи епизода:</h6>
                                        </Typography>
                                        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                                            <div>
                                                {faces.map((face) => (
                                                    <Avatar className={"MuiAvatar-root"} key={face} src={face}/>
                                                ))}
                                            </div>
                                            <LinkContainer
                                                to={`/characters/${item.id}`}
                                                className="mt-3">
                                                <Button variant="light">More</Button>
                                            </LinkContainer>
                                        </div>

                                        <Divider className={"MuiDivider-root"} light/>

                                    </CardContent>
                                </Card>
                            </div>
                        )

                    })
                    : null
                }
            </Masonry>
        </MuiThemeProvider>
            {loading ?
                <div style={{textAlign: 'center'}}>
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </div>
                : null}
            {!loading ?
                <Button
                    variant="outline-dark"
                    onClick={() => loadMorePost()}
                >
                    Load more posts
                </Button>
                : null}
        </>
    )
}


export default Characters;
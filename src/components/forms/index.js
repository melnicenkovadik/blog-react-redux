import React from 'react';
import FormOne from "./Formic";
import FormTwo from "./formtwo";
import FormThree from "./formthree";
import FormFour from "./formfour";

const Forms = props => {

    return (
        <>
            <div style={{border: '1px solid black', padding: '20px', margin: '50px'}}>
                <h1>Formik</h1>
                <FormOne/>
            </div>
            <div style={{border: '1px solid black', padding: '20px', margin: '50px'}}>

                <h1>Formik hooks </h1>
                <FormTwo/>
            </div>
            <div style={{border: '1px solid black', padding: '20px', margin: '50px'}}>

                <h1>Formik custom hook </h1>
                <FormThree/>
            </div>
            <div style={{border: '1px solid black', padding: '20px', margin: '50px'}}>

                <h1>Formik custom hook(recomend) </h1>
                <FormFour/>
            </div>
        </>
    );
};

export default Forms;

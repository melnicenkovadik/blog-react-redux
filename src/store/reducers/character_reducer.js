
import {
    GET_POSTS,
    GET_CHARACTER,
    CLEAR_POST_BY_ID
} from '../types';


export default function postsReducer(state={},action){
    switch(action.type){
        case GET_POSTS:
            return {...state, ...action.payload}
        case GET_CHARACTER:
            return {...state, data: action.payload}
        case CLEAR_POST_BY_ID:
            return {...state, characterId: action.payload}
        default:
            return state
    }
}
import {combineReducers} from 'redux'
import character from './character_reducer'
import user from './user_reducer'

const appReducers = combineReducers({
    character,
    user
})

export default appReducers

import * as api from '../../api';
import {
    GET_POSTS,
    ADD_NEWSLETTER,
    CLEAR_NEWSLETTER,
    GET_POST_BY_ID,
    CLEAR_POST_BY_ID,
    GET_CHARACTER
} from '../types';


/*////////////////////////////////////
            POST
/////////////////////////////////////*/

export const getCharacters = (homePosts, page) => ({
    type: GET_POSTS,
    payload: api.getCharacters(homePosts, page)
})
export const getCharacter = (id) => ({
    type: GET_CHARACTER,
    payload: api.getCharacter(id)
})

export const clearPostById = () => ({
    type: CLEAR_POST_BY_ID,
    payload: {}
})

/*////////////////////////////////////
            USER
/////////////////////////////////////*/

export const addNewsletter = (data) => ({
    type: ADD_NEWSLETTER,
    payload: api.addNewsletter(data)
})

export const clearNewsletter = () => ({
    type: CLEAR_NEWSLETTER,
    payload: {}
})
import React from 'react';
import {Switch, Route, BrowserRouter} from 'react-router-dom'
import Header from "./components/Header";
import MainLayout from "./hoc/MainLayout";
import Forms from "./components/forms";
import CharacterComponent from "./components/Character/Character";
import Characters from "./components/Character/Characters";

const Routes = props => {
    return (
        <BrowserRouter>
            <Header/>
            <MainLayout>
                <Switch>
                    <Route path="/" component={() => (<><h1>Site map for example :)</h1></>)} exact/>
                    <Route path="/characters" component={Characters} exact/>
                    <Route path="/forms" component={Forms} exact/>
                    <Route path="/characters/:id" component={CharacterComponent}/>

                </Switch>
            </MainLayout>

        </BrowserRouter>
    );
};

export default Routes;
